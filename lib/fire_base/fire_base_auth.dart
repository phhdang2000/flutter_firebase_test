import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

class FirAuth {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  void signUp(String email, String password) {
    _firebaseAuth
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((user) {
      _createUser(user.user!.uid, email, password);
    }).catchError((err) {});
  }

  _createUser(String userid, String email, String password) {
    var user = {"email": email};
    var ref = FirebaseDatabase.instance.ref().child("users");
    ref.child(userid).set(user).then((v1) {}).catchError((onError) {});
  }

  void signIn(String email, String pass, Function onSuccess,
      Function(String) onSignInError) {
    _firebaseAuth
        .signInWithEmailAndPassword(email: email, password: pass)
        .then((user) {
      onSuccess();
    }).catchError((err) {});
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }
}
