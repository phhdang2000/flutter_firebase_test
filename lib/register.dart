import 'package:flutter/material.dart';
import 'package:login/fire_base/fire_base_auth.dart';
import 'package:login/main.dart';

class registerPageState extends State<registerPage> {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final repassController = TextEditingController();
  var _firAuth = FirAuth();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passController.dispose();
    repassController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
        tag: 'hero',
        child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 48.0,
            child: Image.asset('assets/logo VHEC.png')));

    final email = TextField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Enter your email...',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      controller: emailController,
    );

    final password = TextField(
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Enter your password...',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      controller: passController,
    );

    final RePassword = TextField(
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Re-enter your password...',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      controller: repassController,
    );

    final loginButton =
        ElevatedButton(child: const Text('Register'), onPressed: register);

    return Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background_login.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          body: Center(
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0),
              children: <Widget>[
                logo,
                SizedBox(height: 45.0),
                email,
                SizedBox(height: 10.0),
                password,
                SizedBox(height: 10.0),
                RePassword,
                SizedBox(height: 15.0),
                loginButton,
              ],
            ),
          ),
        ));
  }

  void register() {
    if (passController.text == repassController.text &&
        passController.text.length >= 6) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Register Success')),
      );
      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
        signUp(emailController.text, passController.text);
        return LoginPage();
      }));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Registration failed')),
      );
    }
  }

  void signUp(String email, String password) {
    _firAuth.signUp(email, password);
  }
}
