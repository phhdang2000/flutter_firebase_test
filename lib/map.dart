import 'package:flutter/material.dart';
import 'package:login/main.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ggMap extends State<googleMap> {
  @override
  Widget build(BuildContext context) {
    final CameraPosition initialPosition = CameraPosition(
      target: LatLng(37.7749, -122.4194), // Tọa độ vị trí ban đầu
      zoom: 12.0, // Mức độ thu phó ban đầu
      tilt: 0, // Góc nghiêng ban đầu (0 là ngang)
      bearing: 0, // Góc xoay ban đầu (0 là hướng Bắc)
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Google Map'),
      ),
      body: GoogleMap(
        initialCameraPosition: initialPosition,
      ),
    );
  }
}
