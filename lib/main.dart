import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:login/Home.dart';
import 'package:login/register.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:login/fire_base/fire_base_auth.dart';
import 'firebase_options.dart';
import 'package:login/map.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Welcome to Flutter",
      home: LoginPage(),
      theme: ThemeData(
        primaryColor: Colors.red,
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  var _firAuth = FirAuth();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
        tag: 'hero',
        child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 48.0,
            child: Image.asset('assets/logo VHEC.png')));

    final email = TextField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Enter your email...',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      controller: emailController,
    );

    final password = TextField(
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Enter your password...',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      controller: passController,
    );

    final loginButton =
        ElevatedButton(child: const Text('Sign In'), onPressed: signIn);

    final forgotLabel = TextButton(
      onPressed: register,
      child: Text(
        "Don't have an account? Sign Up",
        style: TextStyle(color: Colors.black54),
      ),
    );

    return Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background_login.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          body: Center(
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0),
              children: <Widget>[
                logo,
                SizedBox(height: 45.0),
                email,
                SizedBox(height: 10.0),
                password,
                SizedBox(height: 15.0),
                loginButton,
                forgotLabel
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: mappage,
            backgroundColor: Colors.blue,
            child: const Icon(Icons.map),
          ),
        ));
  }

  // void login() {
  //   if (emailController.text == '1' && passController.text == '1') {
  //     Navigator.of(context).push(MaterialPageRoute(builder: (context) {
  //       return HomeScreen();
  //     }));
  //   } else {
  //     ScaffoldMessenger.of(context).showSnackBar(
  //       const SnackBar(content: Text('Incorrect username or password.')),
  //     );
  //   }
  // }

  void signIn() {
    _firAuth.signIn(emailController.text, passController.text, () {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => HomeScreen()));
    }, (msg) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Incorrect username or password.')),
      );
    });
  }

  void register() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return registerPage();
    }));
  }

  void mappage() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return googleMap();
    }));
  }
}

class HomeScreen extends StatefulWidget {
  @override
  RandomWordsState createState() => new RandomWordsState();
}

class registerPage extends StatefulWidget {
  @override
  registerPageState createState() => new registerPageState();
}

class googleMap extends StatefulWidget {
  @override
  ggMap createState() => new ggMap();
}
